# tvo download script

## Requirements

- [docker](https://docs.docker.com/get-docker/)
- make (for windows see [here](http://gnuwin32.sourceforge.net/packages/make.htm)

## Usage

### Build and Push docker image

```bash
make build

make push
```

### Download reports

```bash
make

YEAR=2018 make
```