FROM debian:stable

RUN apt-get update \
  && apt-get install -y curl xz-utils libxrender1 libfontconfig libxext6 python3 python3-pip \
  && rm -rf /var/cache/apt/archives/*

ENV WKHTMLTOX_VERSION 0.12.5
RUN curl -L https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/${WKHTMLTOX_VERSION}/wkhtmltox_${WKHTMLTOX_VERSION}-1.stretch_amd64.deb -o /tmp/wk.deb \
  && apt install -y /tmp/wk.deb  \
  && rm -f  /tmp/wk.deb

WORKDIR /app
ADD requirements.txt /app

RUN pip3 install -r /app/requirements.txt \
  && rm -f /app/requirements.txt

ADD report_download.py /app
ADD report.css /app
RUN chmod +x /app/report_download.py \
  && mkdir /app/data