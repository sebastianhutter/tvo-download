#
# download reports from specified year
#

YEAR ?= 2020
DOCKERENV := $(wildcard /.dockerenv)

define CLI
	docker run --rm -v ${PWD}:/data -w /data \
		registry.gitlab.com/sebastianhutter/tvo-download \
		./report_download.py --year $(YEAR)
	docker cp tvo-download:/app/data/ ./
endef

ifdef DOCKERENV 
$(info running in container, script will be executed directly)
define CLI
	./report_download.py --year $(YEAR)
endef
endif

run:
	$(CLI)

build: Dockerfile requirements.txt report_download.py
	-@docker pull registry.gitlab.com/sebastianhutter/tvo-download 
	@docker build -t registry.gitlab.com/sebastianhutter/tvo-download .

push: 
	@docker push registry.gitlab.com/sebastianhutter/tvo-download 

