#!/usr/bin/env python3

"""
    download articles from the www.tvo.ch
"""


import argparse
from datetime import datetime
import logging

import re





import pdfkit
from requests_html import HTMLSession
import string

import os
import sys


##
# GOBALS
##

BASE_URL="https://www.tvo.ch"
BERICHTE_URI="/index.php/fotos-berichte".format(BASE_URL)

##
# FUNCTIONS
##

##
# CLASSES
##

class ReportsList(object):
    def __init__(self, url, item_identifier=".cck_page_items", list_identifier='.listItem'):
        """
            retrieve report list
        """ 
        super().__init__()

        self.url = url
        self.item_identifier = item_identifier
        self.list_identifier = list_identifier

        logging.info("Retrieve page items identified by '{}'".format(self.item_identifier))
        self.page_items = self._get_page_items()

        logging.info("Get report links from page items")
        self.links = self._get_report_links_from_page_items()
        logging.info("Found {} reports!".format(len(self.links)))

    def _get_page_items(self):
        """
            retrieve html content inside div identified 
            by class identifier
        """
        session = HTMLSession()
        r = session.get(self.url)
        page_items = r.html.find(self.item_identifier, first=True)

        if not page_items:
            raise ValueError("Unable to retrieve page content in div identified by {}".format(self.item_identifier))
        else:
            return page_items

    def _get_report_links_from_page_items(self):
        """
            retrieve all report links from the page items.
            
            unfortunately we can't just use ".absolute_links" as the links to the report pages
            are java script. the link is 

            ```
            <div class="listItem" onclick="window.open('/index.php/fotos-berichte/188-toni-und-der-samichlaus','_self');">
            ```
            (example)
        """
        
        # regular expression to match strings like
        # '/index.php/fotos-berichte/135-trainingstag'
        regular_expression = re.compile('\'({}/.+?)\''.format(BERICHTE_URI))
        report_links = []
        # find all list items and loop trough them
        # check if they contain an "onclick" attribute
        # if so create full url from content
        for item in self.page_items.find(self.list_identifier):
            if 'onclick' in item.attrs:
                m = regular_expression.search(item.attrs['onclick'])
                if m:
                    report_links.append("{}{}".format(BASE_URL, m.group(1)))

        if not report_links:
            raise("Unable to retrieve any report links from report page")
        
        return report_links

class ReportPage(object):
    def __init__(self, url, body_identifier=".beitrag", title_identifier='.title', info_identifier='.info', event_label='Anlass', date_label='Datum', thumbnail_size="200"):
        """
            retrieve page content and meta information
            render pdf from template
        """
        super().__init__()

        self.url = url
        self.body_identifier = body_identifier
        self.title_identifier = title_identifier
        self.info_identifier = info_identifier
        self.event_label = event_label
        self.date_label = date_label

        logging.info("Get page body for {}, identified by {}".format(self.url, self.body_identifier))
        self.page_body_raw = self._get_page_body()

        logging.info("Retrieve meta information from page body")
        self.title = self._get_title()
        self.event = self._get_event()
        self.date = self._get_date()

        logging.info("Replace relative image paths with absolute image paths")
        self.page_body = self.page_body_raw.html.replace('src="/images/', '<img src="{}/images/'.format(BASE_URL))
        logging.info("Set thumbnail size from 200 to {}".format(thumbnail_size))
        self.page_body = self.page_body.replace('width=200&amp;height=200&amp;', 'width={}&amp;height={}&amp;'.format(thumbnail_size, thumbnail_size))

        logging.info("Create usable filename for pdf export")
        self.filename = self._generate_filename()

    def _get_page_body(self):
        """
            return the whole page body, excluding the custom css style (we need to create a custom one for print)
            identified by the body_identifier
        """

        session = HTMLSession()
        r = session.get(self.url)
        page_body = r.html.find(self.body_identifier, first=True)

        if not page_body:
            raise ValueError("Unable to retrieve page body in div identified by {}".format(self.body_identifier))
        else:
            return page_body


    def _get_title(self):
        """
            return the page title
        """
        title = self.page_body_raw.find(self.title_identifier, first=True)

        if not title:
            raise ValueError("Unable to retrieve title identified by {}".format(self.title_identifier))
        else:
            return title.text

    def _get_event(self):
        """ 
            return the event name
        """

        event = self._get_value_from_info(self.event_label)
        if not event:
            # in some cases there is no event so we just replace event with title
            logging.warn("Unable to retrieve event name identified by {} will use reports title instead".format(self.event_label))
            return self.title
            #raise ValueError("Unable to retrieve event name identified by {}".format(self.event_label))
        return event

    def _get_date(self):
        """
            return the date of the event
        """
        date = self._get_value_from_info(self.date_label)
        if not date:
            raise ValueError("Unable to retrieve date identified by {}".format(self.date_label))
        return datetime.strptime(date, '%d.%m.%Y')


    def _get_value_from_info(self, label):
        """
            return the value identified by the lable in the "info" box
            of an event

            events, date, authors etc are in a funny html list inside headItem:

            ```
            <div class="beitrag_head">
		        <div class="headItem img">
			        <p><img src="/images/Benutzerdaten/Fotos/2019/19-samichlaus/19-chlaus07.jpg" alt="19 chlaus07" /></p>
                </div>
		        <div class="headItem">
			        <p class='info'>
                        <div class='label'>Anlass:</div>
                        <div class='value'>Samichlaus</div>
                    </p>
                    <p class='info'>
                        <div class='label'>Riege: </div>
                        <div class='value'>Jugendriege, Mädchenriege, Faustball</div>
                    </p>
                    <p class='info'>
                        <div class='label'>Ort: </div>
                        <div class='value'>Oberwinterthur</div>
                    </p>
                    <p class='info'>
                        <div class='label'>Datum: </div>
                        <div class='value'>13.12.2019</div>
                    </p>
                    <p class='info'>
                        <div class='label'>Autor: </div>
                        <div class='value'>H. von Mengden</div>
                    </p>
		        </div>
	        </div>
            ```
        """

        info_list = self.page_body_raw.find(self.info_identifier)
        if not info_list:
            raise ValueError("Unable to retrieve meta information from report page")

        for index, info in enumerate(info_list):
            info_label = info.find('.label', first=True)
            info_value = info.find('.value', first=True)
            
            if info_label.text.startswith(label):
                return info_value.text

        return None

    def _generate_filename(self):
        """
            create a valid filename from date and title
        """

        filename = "{}_{}_{}.pdf".format(self.date.strftime('%Y%m%d'), self.event, self.title)
        return self._format_filename(filename)
    

    def _format_filename(self, s):
        # https://gist.github.com/seanh/93666
        valid_chars = "-_.()öäü %s%s" % (string.ascii_letters, string.digits)
        filename = ''.join(c for c in s if c in valid_chars)
        filename = filename.replace(' ','_')
        return filename


class PdfReport(object):
    def __init__(self, content, footer_title, filename, destination_directory="./data"):
        """
            generate pdf file and store it on disk
        """
        super().__init__()

        self.content = """
            <html>
                <head>
                    <meta name="pdfkit-page-size" content="A4"/>
                    <meta name="pdfkit-orientation" content="Portrait"/>
                    <meta name="pdfkit-footer-line" content=""/>
                    <meta name="pdfkit-footer-right" content="[page] of [topage]"/>
                    <meta charset="utf-8">
                    <link rel="stylesheet" href="/app/report.css">
                </head>
                <body>
                    {}
                </body>
            </html>
        """.format(content)
        
        self.file = os.path.join(destination_directory, filename)

        self.options = {
        'page-size': 'A4',
        'orientation': 'Portrait',
        'margin-top': '0.5in',
        'margin-right': '0.75in',
        'margin-bottom': '0.5in',
        'margin-left': '0.75in',
        'encoding': "UTF-8",
        'footer-left': footer_title,
        'footer-line':'',
        'footer-font-size':'8',
        'footer-right': 'Seite [page] von [topage]',

        'custom-header' : [
            ('Accept-Encoding', 'gzip')
        ],
        'no-outline': None
    }

    def export(self):
        """
            export the pdf file
        """ 
        pdfkit.from_string(self.content, self.file, options=self.options)

        # write html files for debugging
        with open('{}.htm'.format(self.file), 'w') as f:
            f.write(self.content)

## 
# MAIN
##

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')

    # retrieve year from envrionment variable and allow for argument parsing
    year_parameter = os.getenv('YEAR', datetime.now().year)
    parser = argparse.ArgumentParser(description="Download reports from www.tvo.ch")
    parser.add_argument("--year", "-y", default=year_parameter, help="Year to retrieve reports from (defaults to: {}".format(year_parameter))
    args = parser.parse_args()

    try: 
        report_list = ReportsList("{}{}?cck=beitrag&year_searchbox_eventdate={}".format(BASE_URL, BERICHTE_URI,args.year))
        for l in report_list.links:
            p = ReportPage(l)
            pdf = PdfReport(p.page_body, p.filename, p.filename)
            pdf.export() 
    except:
        logging.exception("Unhandled exception")
        sys.exit(1)